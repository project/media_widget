Media Widget
------------

Defines basic upload and URL widgets for media types not allowing for reuse.
These mimic the core File and Link widgets. This allows to use Media as a
replacement for plain File entities.
