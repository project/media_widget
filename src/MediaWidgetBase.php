<?php

namespace Drupal\media_widget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityFormModeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for media widgets.
 */
abstract class MediaWidgetBase extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The updated media repository.
   *
   * @var \Drupal\media_widget\UpdatedMediaRepository
   */
  protected $updatedMediaRepository;

  /**
   * Constructs a new MediaWidgetBase.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\media_widget\UpdatedMediaRepository $updated_media_repository
   *   The updated media repository.
   */
  public function __construct(
    string $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager,
    UpdatedMediaRepository $updated_media_repository
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
    $this->updatedMediaRepository = $updated_media_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('media_widget.update_media_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return
      $field_definition->getSetting('target_type') === 'media' &&
      static::getMediaTypes($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + [
      'image_style' => 'thumbnail',
      'form_mode' => static::getDefaultMediaFormMode(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $image_styles = $this->entityTypeManager
      ->getStorage('image_style')
      ->loadMultiple();

    $options = array_map(
      function (ImageStyle $image_style) { return $image_style->label(); },
      $image_styles
    );

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Preview image style'),
      '#default_value' => $this->getSetting('image_style'),
      '#options' => $options,
    ];

    $form_modes = $this->entityTypeManager
      ->getStorage('entity_form_mode')
      ->loadMultiple();

    $options = array_combine(
      array_map(
        function (EntityFormModeInterface $form_mode) {
          return preg_replace('/^media\./', '', $form_mode->id());
        },
        $form_modes
      ),
      array_map(
        function (EntityFormModeInterface $form_mode) {
          return $form_mode->label();
        },
        $form_modes
      )
    );

    $form['form_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Form mode'),
      '#default_value' => $this->getSetting('form_mode'),
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $image_style_id = $this->getSetting('image_style');
    $image_style = $this->entityTypeManager
      ->getStorage('image_style')
      ->load($image_style_id);

    $form_mode_id = 'media.' . $this->getSetting('form_mode');
    $form_mode = $this->entityTypeManager
      ->getStorage('entity_form_mode')
      ->load($form_mode_id);

    return [
      'image_style' => $this->t('Image style:  @label', ['@label' => $image_style->label()]),
      'form_mode' => $this->t('Form mode:  @label', ['@label' => $form_mode->label()]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#type'] = 'fieldset';

    $entity = $items->getEntity();
    $state = $this->getState($form, $form_state);
    $state['entity_type_id'] = $entity->getEntityTypeId();
    $state['bundle'] = $entity->bundle();
    $state['entity_id'] = $entity->id();
    $state['entity_uuid'] = $entity->uuid();
    $state['form_mode'] = $this->getSetting('form_mode');
    $state['ajax_wrapper_id'] = Html::getUniqueId('media-upload-wrapper-' . $this->fieldDefinition->getName());
    $this->setState($form, $form_state, $state);

    $element['#ajax_wrapper_id'] = $state['ajax_wrapper_id'];
    $element['#prefix'] = '<div id="' . $state['ajax_wrapper_id'] . '">';
    $element['#suffix'] = '</div>';

    $media_items = $this->getMediaItems($form, $form_state, $items);

    $description = $this->fieldDefinition->getDescription();
    if ($description) {
      $element['description'] = [
        '#theme_wrappers' => ['container__media_widget_description'],
        'media_widget_description' => [
          '#markup' => $description,
        ],
      ];
      $element['#description'] = '';
    }

    if ($media_items) {
      $element['#attributes']['data-input'] = 'true';

      // This deserves to be themeable, but it doesn't need to be its own "real"
      // template.
      $element['status'] = [
        '#type' => 'inline_template',
        '#template' => '<p>{{ text }}</p>',
        '#context' => [
          'text' => $this->formatPlural(count($media_items), 'The media item has been created but has not yet been saved.', 'The media items have been created but have not yet been saved.'),
        ],
      ];

      $element['media'] = [
        '#attributes' => [
          'aria-label' => $this->t('Added media items'),
          'tabindex' => '-1',
        ],
      ];
      foreach ($media_items as $delta => $media) {
        $element['media'][$delta] = $this->buildEntityFormElement($media, $form, $form_state, $delta);
      }
    }

    $cardinality = $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED || count($media_items) < $cardinality) {
      $element = $this->buildInputElement($element, $form_state, $form);
    }

    $element['#element_validate'][] = [static::class, 'validateMediaItems'];

    return $element;
  }

  /**
   * Builds the element for submitting source field value(s).
   *
   * @param array $element
   *   The widget form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The form array.
   *
   * @return array
   *   The widget element, with the "add media" element added.
   */
  abstract protected function buildInputElement(array $element, FormStateInterface $form_state, array $form): array;

  /**
   * Builds the sub-form for setting required fields on a new media item.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A new, unsaved media item.
   * @param array $form
   *   The complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param int $delta
   *   The delta of the media item.
   *
   * @return array
   *   The element containing the required fields sub-form.
   */
  protected function buildEntityFormElement(MediaInterface $media, array $form, FormStateInterface $form_state, int $delta): array {
    $state = $this->getState($form, $form_state);
    $parents = $this->getMediaFormElementParents($form, $delta);

    $element = [
      '#type' => 'container',
      '#attributes' => [
        'aria-label' => $media->getName(),
        'tabindex' => '-1',
        'data-media-widget-added-delta' => $delta,
      ],
      'preview' => [
        '#type' => 'container',
        '#weight' => 10,
      ],
      'fields' => [
        '#type' => 'container',
        '#weight' => 20,
        // The '#parents' are set here because the entity form display needs it
        // to build the entity form fields.
        '#parents' => array_merge($parents, ['fields']),
      ],
      'remove_button' => [
        '#type' => 'submit',
        '#delta' => $delta,
        '#field_name' => $this->fieldDefinition->getName(),
        '#value' => $this->t('Remove'),
        // We need to make sure each button has a unique name attribute. The
        // default name for button elements is 'op'. If the name is not unique,
        // the triggering element is not set correctly and the wrong media item
        // is removed.
        '#name' => $this->getRemoveButtonName($form, $delta),
        '#weight' => 30,
        '#attributes' => [
          'aria-label' => $this->t('Remove @label', ['@label' => $media->getName()]),
        ],
        '#ajax' => [
          'callback' => [static::class, 'updateWidgetElement'],
          'wrapper' => $state['ajax_wrapper_id'],
          'message' => $this->t('Removing @label.', ['@label' => $media->getName()]),
        ],
        // Ensure errors in other media items do not prevent removal.
        '#limit_validation_errors' => [],
        '#element_validate' => [
          [static::class, 'validateRemoveElement'],
        ],
        // We need the "#submit" key to be defined, otherwise the validation
        // error limitation settings will not be applied.
        '#submit' => [],
      ],
    ];

    $source = $media->getSource();
    $plugin_definition = $source->getPluginDefinition();
    if ($thumbnail_uri = $source->getMetadata($media, $plugin_definition['thumbnail_uri_metadata_attribute'])) {
      $element['preview']['thumbnail'] = [
        '#theme' => 'image_style',
        '#style_name' => $this->getSetting('image_style'),
        '#uri' => $thumbnail_uri,
      ];
    }

    $form_mode = static::getMediaFormMode($this->fieldDefinition->getName(), $form, $form_state);
    $form_display = EntityFormDisplay::collectRenderDisplay($media, $form_mode);
    // When the name is not added to the form as an editable field, output
    // the name as a fixed element to confirm the right item was added.
    if (!$form_display->getComponent('name')) {
      $element['fields']['name'] = [
        '#type' => 'item',
        '#title' => $this->t('Name'),
        '#markup' => $media->getName(),
      ];
    }
    $form_display->buildForm($media, $element['fields'], $form_state);

    // Add source field name so that it can be identified in form alter and
    // widget alter hooks.
    $element['fields']['#source_field_name'] = $this->getSourceFieldName($media->get('bundle')->entity);

    // The revision log field is currently not configurable from the form
    // display, so hide it by changing the access.
    if (isset($element['fields']['revision_log_message'])) {
      $element['fields']['revision_log_message']['#access'] = FALSE;
    }

    return $element;
  }

  /**
   * Validate media item values.
   *
   * @param array $element
   *   The form element to be validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The complete form array.
   */
  public static function validateMediaItems(array $element, FormStateInterface $form_state, array $form): void {
    if (!empty($element['#removing'])) {
      return;
    }

    $state = static::getWidgetState($element['#field_parents'], $element['#field_name'], $form_state);
    foreach ($state['items'] ?? [] as $delta => $media) {
      assert($media instanceof MediaInterface);
      $media_element = $element['media'][$delta]['fields'] ?? [];
      if ($media_element) {
        $form_mode = static::getMediaFormMode($element['#field_name'], $form, $form_state);
        $form_display = EntityFormDisplay::collectRenderDisplay($media, $form_mode);
        $form_display->extractFormValues($media, $media_element, $form_state);
        $form_display->validateFormValues($media, $media_element, $form_state);
      }
    }
  }

  /**
   * Validates media removal.
   *
   * @param array $element
   *   The form element to be validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The complete form array.
   */
  public static function validateRemoveElement(array $element, FormStateInterface $form_state, array $form): void {
    if ($element['#name'] !== $form_state->getTriggeringElement()['#name']) {
      return;
    }

    // We need to massage form input and values, otherwise the wrong ones will
    // be applied once the widget is rebuilt, since deltas wil not match.
    $parents = array_merge($form['#parents'], [$element['#field_name']]);
    $values = &NestedArray::getValue($form_state->getValues(), $parents, $key_exists);
    if ($key_exists) {
      unset($values['media'][$element['#delta']]);
      $values['media'] = array_values($values['media']);
      $input = &NestedArray::getValue($form_state->getUserInput(), $parents);
      unset($input['media'][$element['#delta']]);
      $input['media'] = array_values($input['media']);
    }

    // We need to signal that a remove is in process, to avoid processing user
    // input.
    $widget_element = &static::getWidgetElement($form, $form_state);
    $widget_element['#removing'] = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $item_values = [];

    // Handle item removal.
    $triggering_element = $form_state->getTriggeringElement();
    $delta = $triggering_element['#delta'] ?? NULL;
    if (isset($delta) && $triggering_element['#name'] === $this->getRemoveButtonName($form, $delta)) {
      $this->removeMediaItem($delta, $form, $form_state);
    }
    else {
      // Handle item additions.
      $added_items = $this->processInputValues($values, $form, $form_state);
      if ($added_items) {
        $items = $this->getMediaItems($form, $form_state);
        $items = array_merge($items, $added_items);
        $this->setMediaItems($form, $form_state, $items);
        if ($this->needsRebuilding($form, $form_state)) {
          $form_state->setRebuild();
        }
      }

      // Update media items with the submitted values extracted in the validate
      // phase.
      $state = $this->getState($form, $form_state);
      $field_name = $this->fieldDefinition->getName();
      $this->updatedMediaRepository->clearEntityMediaIds($state['entity_type_id'], $state['entity_uuid'], $field_name);
      foreach ($this->getMediaItems($form, $form_state) as $delta => $media) {
        // New media items will be saved in EntityReferenceItem::preSave(), we
        // need to keep track of updated media items, so we can save them
        // later as well. Doing that in the presave phase ensures that they
        // are all saved within the same transaction.
        if (!$media->isNew()) {
          $this->updatedMediaRepository->addMediaId($state['entity_type_id'], $state['entity_uuid'], $field_name, $media->id());
        }
        $item_values[$delta] = ['entity' => $media];
      }
    }

    return $item_values;
  }

  /**
   * Checks whether the widget needs rebuilding after adding new items.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if the widget should be rebuilt, FALSE otherwise.
   */
  abstract protected function needsRebuilding(array $form, FormStateInterface $form_state): bool;

  /**
   * Creates media items from source field input values.
   *
   * @param array $values
   *   The widget values.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return \Drupal\media\MediaInterface[]
   *   The newly created media items.
   */
  abstract protected function processInputValues(array $values, array $form, FormStateInterface $form_state): array;

  /**
   * Creates a new, unsaved media item from a source field value.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type of the media item.
   * @param mixed $value
   *   The value for the source field of the media item.
   *
   * @return \Drupal\media\MediaInterface
   *   An unsaved media entity.
   */
  protected function createMediaFromValue(MediaTypeInterface $media_type, $value): MediaInterface {
    $media_storage = $this->entityTypeManager
      ->getStorage('media');

    $source_field_name = $this->getSourceFieldName($media_type);

    $media = $media_storage->create([
      'bundle' => $media_type->id(),
      $source_field_name => $value,
    ]);
    assert($media instanceof MediaInterface);
    $media->setName($media->getName());

    return $media;
  }

  /**
   * Submit handler for the remove button.
   *
   * @param int $delta
   *   The removed item delta.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function removeMediaItem(int $delta, array $form, FormStateInterface $form_state): void {
    $items = $this->getMediaItems($form, $form_state);
    $removed_media = $items[$delta] ?? NULL;

    if ($removed_media) {
      unset($items[$delta]);
      $this->setMediaItems($form, $form_state, array_values($items));

      // Show a message to the user to confirm the media is removed.
      $this->messenger()->addStatus($this->t('The media item %label has been removed.', ['%label' => $removed_media->label()]));
    }

    $form_state->setRebuild();
  }

  /**
   * AJAX callback to update the entire form based on source field input.
   *
   * @param array $form
   *   The complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The widget element array.
   */
  public static function updateWidgetElement(array $form, FormStateInterface $form_state): array {
    return static::getWidgetElement($form, $form_state);
  }

  /**
   * Get the media type from the form state.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The widget field definition.
   *
   * @return \Drupal\media\MediaTypeInterface[]
   *   The supported media types.
   */
  protected static function getMediaTypes(FieldDefinitionInterface $field_definition): array {
    $ids = array_keys($field_definition->getSetting('handler_settings')['target_bundles']);
    $all_media_types = \Drupal::entityTypeManager()
      ->getStorage('media_type')
      ->loadMultiple($ids);

    $media_types = [];
    foreach ($all_media_types as $id => $media_type) {
      if (!($media_type instanceof MediaTypeInterface)) {
        throw new \InvalidArgumentException(sprintf('The "%s" media type does not exist', $id));
      }
      if (!static::isSupportedMediaType($media_type)) {
        continue;
      }
      $media_types[$id] = $media_type;
    }

    return $media_types;
  }

  /**
   * Checks whether a media type is supported by the current widget.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   A media type entity.
   *
   * @return bool
   *   TRUE if the media type is supported, FALSE otherwise.
   */
  abstract protected static function isSupportedMediaType(MediaTypeInterface $media_type): bool;

  /**
   * Returns the definition of the widget field.
   *
   * @param array $element
   *   The field widget element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   A field definition.
   */
  protected static function getFieldDefinition(array $element, FormStateInterface $form_state): FieldDefinitionInterface {
    $field_name = $element['#field_name'];
    $state = static::getWidgetState($element['#field_parents'] ?? [], $field_name, $form_state);
    if (isset($state['entity_type_id']) && isset($state['bundle'])) {
      $entity_field_manager = \Drupal::service('entity_field.manager');
      assert($entity_field_manager instanceof EntityFieldManagerInterface);
      $field_definitions = $entity_field_manager->getFieldDefinitions($state['entity_type_id'], $state['bundle']);
      return $field_definitions[$field_name];
    }
    throw new \LogicException(sprintf('Invalid entity type: %s', $state['entity_type_id'] ?? $field_name));
  }

  /**
   * Returns the widget element.
   *
   * @param array $form
   *   The complete form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string|null $field_name
   *   (optional) The widget field name. By default it is derived by the
   *   triggering element.
   *
   * @return array
   *   A form element array.
   */
  protected static function &getWidgetElement(array &$form, FormStateInterface $form_state, string $field_name = NULL): array {
    $element = [];
    $triggering_element = $form_state->getTriggeringElement();
    $field_name = $field_name ?? $triggering_element['#field_name'] ?? NULL;
    if (!$field_name) {
      return $element;
    }

    $index = array_search($field_name, $triggering_element['#array_parents']);
    if ($index === FALSE) {
      return $element;
    }

    // We want to drill down to the widget element, so we add one for the
    // widget container and for the widget itself (index is 0-based).
    $parents = array_slice($triggering_element['#array_parents'], 0, $index + 2);
    $element = &NestedArray::getValue($form, $parents);
    return $element;
  }

  /**
   * Returns the widget media items.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\Core\Field\FieldItemListInterface|null $item_list
   *   (optional) The initial field value. Defaults to none.
   *
   * @return \Drupal\media\MediaInterface[]
   *   An array containing all uploaded and existing media items.
   */
  protected function getMediaItems(array $form, FormStateInterface $form_state, FieldItemListInterface $item_list = NULL): array {
    $state = $this->getState($form, $form_state);
    $items = &$state['items'];

    if (!isset($items)) {
      if (isset($item_list)) {
        $value = $item_list->getValue();
        $ids = array_filter(array_map(function ($value) { return $value['target_id'] ?? NULL; }, $value));
        $items = array_filter(array_map(function ($value) { return $value['entity'] ?? NULL; }, $value));
        if ($ids) {
          $storage = $this->entityTypeManager->getStorage('media');
          /** @var \Drupal\media\MediaInterface[] $items */
          $items = array_merge($items, $storage->loadMultiple($ids));
        }
        if ($items) {
          $this->setMediaItems($form, $form_state, $items);
        }
      }
    }

    return $items ?? [];
  }

  /**
   * Sets the widget media items.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\media\MediaInterface[] $items
   *   The media items.
   */
  protected function setMediaItems(array $form, FormStateInterface $form_state, array $items): void {
    $state = $this->getState($form, $form_state);
    $state['items'] = $items;
    $state['items_count'] = count($items);
    $this->setState($form, $form_state, $state);
  }

  /**
   * Returns the widget state.
   *
   * @param array $form
   *   The complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The widget state.
   */
  protected function getState(array $form, FormStateInterface $form_state): array {
    return (array) static::getWidgetState($form['#parents'] ?? [], $this->fieldDefinition->getName(), $form_state);
  }

  /**
   * Sets the widget state.
   *
   * @param array $form
   *   The complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $state
   *   The widget state.
   */
  protected function setState(array $form, FormStateInterface $form_state, array $state): void {
    static::setWidgetState($form['#parents'] ?? [], $this->fieldDefinition->getName(), $form_state, $state);
  }

  /**
   * Returns the form mode to be used by media forms.
   *
   * @param string $field_name
   *   The widget field name.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   The form mode ID.
   */
  protected static function getMediaFormMode(string $field_name, array $form, FormStateInterface $form_state): string {
    $state = static::getWidgetState($form['#parents'] ?? [], $field_name, $form_state);
    return $state['form_mode'] ?? static::getDefaultMediaFormMode();
  }

  /**
   * Returns the default form mode to be used by media forms.
   *
   * @return string
   *   The form mode ID.
   */
  abstract static protected function getDefaultMediaFormMode();

  /**
   * Returns the name of the source field for a media type.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type to get the source field name for.
   *
   * @return string
   *   The name of the media type's source field.
   */
  protected function getSourceFieldName(MediaTypeInterface $media_type): string {
    return $media_type->getSource()
      ->getSourceFieldDefinition($media_type)
      ->getName();
  }

  /**
   * Returns the name to be used for a "Remove" button.
   *
   * @param array $form
   *   The form array.
   * @param int $delta
   *   The delta of the item to be removed.
   *
   * @return string
   *   The remove button name.
   */
  protected function getRemoveButtonName(array $form, int $delta): string {
    $parents = $this->getMediaFormElementParents($form, $delta);
    return implode('-', $parents) . '-remove-button';
  }

  /**
   * Returns the parent section names of media form elements.
   *
   * @param array $form
   *   The form array.
   * @param int $delta
   *   The delta of the item to be removed.
   *
   * @return string[]
   *   An array of section items.
   */
  protected function getMediaFormElementParents(array $form, int $delta): array {
    return array_merge($form['#parents'] ?? [], [$this->fieldDefinition->getName(), 'media', $delta]);
  }

}
