<?php

namespace Drupal\media_widget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Repository for media items updated via the media upload widget.
 *
 * @internal
 */
class UpdatedMediaRepository {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The repository storage.
   *
   * @var \SplObjectStorage
   */
  protected $storage;

  /**
   * UpdatedMediaRepository constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * Adds a media item to the repository.
   *
   * @param string $entity_type_id
   *   The referencing entity type ID.
   * @param string $uuid
   *   The referencing entity UUID.
   * @param string $field_name
   *   The media reference field name.
   * @param int $media_id
   *   The updated media item ID.
   */
  public function addMediaId(string $entity_type_id, string $uuid, string $field_name, int $media_id): void {
    $storage = &$this->getStorage();
    $storage[$entity_type_id][$uuid][$field_name][$media_id] = $media_id;
  }

  /**
   * Removes a media item from the repository.
   *
   * @param string $entity_type_id
   *   The referencing entity type ID.
   * @param string $uuid
   *   The referencing entity UUID.
   * @param string $field_name
   *   The media reference field name.
   * @param int $media_id
   *   The updated media item ID.
   */
  public function removeMediaId(string $entity_type_id, string $uuid, string $field_name, int $media_id): void {
    $storage = &$this->getStorage();
    unset($storage[$entity_type_id][$uuid][$field_name][$media_id]);
  }

  /**
   * Returns all the updated media items for the specified entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The referencing entity.
   *
   * @return int[][]
   *   An associative array of arrays of media IDs keyed by field name.
   */
  public function getEntityMediaIds(FieldableEntityInterface $entity): array {
    $storage = $this->getStorage();
    return array_filter($storage[$entity->getEntityTypeId()][$entity->uuid()] ?? []);
  }

  /**
   * Clears all the updated media items for the specified entity field.
   *
   * @param string $entity_type_id
   *   The referencing entity type ID.
   * @param string $uuid
   *   The referencing entity UUID.
   * @param string $field_name
   *   The entity reference field name.
   */
  public function clearEntityMediaIds(string $entity_type_id, string $uuid, string $field_name): void {
    $storage = &$this->getStorage();
    unset($storage[$entity_type_id][$uuid][$field_name]);
  }

  /**
   * Returns the repository storage.
   *
   * @return array
   *   An associative array.
   */
  protected function &getStorage(): array {
    if (!isset($this->storage)) {
      $this->storage = new \SplObjectStorage();
    }
    $request = $this->requestStack->getCurrentRequest();
    if (!isset($this->storage[$request])) {
      $this->storage[$request] = (object) [
        'data' => [],
      ];
    }
    return $this->storage[$request]->data;
  }

}
