<?php

namespace Drupal\media_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\OEmbed\ResourceException;
use Drupal\media\OEmbed\ResourceFetcherInterface;
use Drupal\media\OEmbed\UrlResolverInterface;
use Drupal\media\Plugin\media\Source\OEmbedInterface;
use Drupal\media_widget\MediaWidgetBase;

/**
 * Plugin implementation of the Media OEmbed widget.
 *
 * @FieldWidget(
 *   id = "media_widget_oembed",
 *   label = @Translation("Remote media widget"),
 *   multiple_values = TRUE,
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class OEmbedMediaWidget extends MediaWidgetBase {

  /**
   * {@inheritdoc}
   */
  protected static function getDefaultMediaFormMode(): string {
    return 'media_widget_remote';
  }

  /**
   * {@inheritdoc}
   */
  protected function buildInputElement(array $element, FormStateInterface $form_state, array $form): array {
    $state = $this->getState($form, $form_state);

    $providers = [];
    $media_types = static::getMediaTypes($this->fieldDefinition);
    foreach ($media_types as $media_type) {
      $source = $media_type->getSource();
      if ($source instanceof OEmbedInterface) {
        $providers = array_merge($providers, $source->getProviders());
      }
    }
    $providers = array_unique($providers);
    asort($providers);

    // Add a remote to group the input elements for styling purposes.
    $element['remote'] = [
      '#theme_wrappers' => ['container__media__item'],
    ];

    $element['remote']['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#description' => $this->t('Allowed providers: @providers.', [
        '@providers' => implode(', ', $providers),
      ]),
      '#required' => $this->fieldDefinition->isRequired() && count($state['items'] ?? []) === 0,
      '#attributes' => [
        'placeholder' => 'https://',
      ],
    ];

    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    $limit_validation_errors = [array_merge($parents, [$field_name])];

    $element['remote']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#name' => $this->getAddButtonName($form),
      '#field_name' => $this->fieldDefinition->getName(),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => [static::class, 'updateWidgetElement'],
        'wrapper' => $state['ajax_wrapper_id'],
      ],
      '#submit' => [[static::class, 'addItems']],
      // Prevent errors in other widgets from affecting this widget.
      '#limit_validation_errors' => $limit_validation_errors,
    ];

    $element['#element_validate'][] = [static::class, 'validateInputElement'];

    return $element;
  }

  /**
   * Does nothing.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function addItems(array $form, FormStateInterface $form_state) {
    // This is here just because #limit_validation_errors will be ignored by
    // FormValidator::determineLimitValidationErrors if #submit is not set.
  }

  /**
   * Validates the oEmbed URL.
   *
   * @param array $element
   *   The form element to be validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The complete form array.
   */
  public static function validateInputElement(array $element, FormStateInterface $form_state, array $form): void {
    $url = static::getUrl($element, $form_state, $form);
    if ($url) {
      try {
        $resource_url = static::getUrlResolver()->getResourceUrl($url);
        static::getResourceFetcher()->fetchResource($resource_url);
      }
      catch (ResourceException $e) {
        $form_state->setError($element['remote']['url'], $e->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function processInputValues(array $values, array $form, FormStateInterface $form_state): array {
    $url = $values['remote']['url'] ?? NULL;
    if (!$url) {
      return [];
    }

    $media_types = static::getMediaTypes($this->fieldDefinition);

    try {
      $media_type = static::getMediaTypeFromUrl($url, $media_types);
    }
    catch (\InvalidArgumentException $e) {
      return [];
    }

    $media = $this->createMediaFromValue($media_type, $url);

    // Reset the widget URL once it has been processed.
    $parents = array_merge($form['#parents'], [$this->fieldDefinition->getName(), 'remote', 'url']);
    foreach (['getValues', 'getUserInput'] as $method) {
      NestedArray::setValue($form_state->{$method}(), $parents, '');
    }

    return [$media];
  }

  /**
   * {@inheritdoc}
   */
  protected function setMediaItems(array $form, FormStateInterface $form_state, array $items): void {
    $triggering_element = $form_state->getTriggeringElement();
    if (!$triggering_element) {
      // This means the form is being loaded as part of a page load.
      parent::setMediaItems($form, $form_state, $items);
      return;
    }
    $element_name = $triggering_element['#name'] ?? NULL;
    $submitted = $element_name === 'op' ||
      $element_name === $this->getAddButtonName($form) ||
      $element_name === $this->getRemoveButtonName($form, $triggering_element['#delta'] ?? 0);
    // Prevent other validation/submit handlers from setting media items.
    if ($submitted) {
      parent::setMediaItems($form, $form_state, $items);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function needsRebuilding(array $form, FormStateInterface $form_state): bool {
    $element = $form_state->getTriggeringElement();
    return $element['#name'] === $this->getAddButtonName($form);
  }

  /**
   * Returns the URL entered by the user.
   *
   * @param array $element
   *   The widget element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The complete form.
   *
   * @return string|null
   *   The user-submitted URL or NULL if none was found.
   */
  protected static function getUrl(array $element, FormStateInterface $form_state, array $form): ?string {
    $field_name = $element['#field_name'];
    $parents = array_merge($form['#parents'], [$field_name, 'remote', 'url']);
    return NestedArray::getValue($form_state->getValues(), $parents) ?: NULL;
  }

  /**
   * Returns a media type for the specified URL.
   *
   * @param string $url
   *   The oEmbed URL.
   * @param \Drupal\media\MediaTypeInterface[] $media_types
   *   The supported media types.
   *
   * @return \Drupal\media\MediaTypeInterface
   *   The first media type matching the URL provider.
   *
   * @throws \InvalidArgumentException
   *   When the URL has no matching provider.
   */
  protected static function getMediaTypeFromUrl(string $url, array $media_types): MediaTypeInterface {
    $provider = '';

    try {
      $provider = static::getUrlResolver()
        ->getProviderByUrl($url)
        ->getName();
    }
    catch (\Exception $e) {
      // We will rethrow this later.
    }

    foreach ($media_types as $media_type) {
      $source = $media_type->getSource();
      if ($source instanceof OEmbedInterface && in_array($provider, $source->getProviders(), TRUE)) {
        return $media_type;
      }
    }

    throw new \InvalidArgumentException(sprintf('Invalid media URL: %s', $url), 0, $e ?? NULL);
  }

  /**
   * {@inheritdoc}
   */
  protected static function isSupportedMediaType(MediaTypeInterface $media_type): bool {
    return $media_type->getSource() instanceof OEmbedInterface;
  }

  /**
   * Returns the name to be used for a "Add" button.
   *
   * @param array $form
   *   The form array.
   *
   * @return string
   *   The remove button name.
   */
  protected function getAddButtonName(array $form): string {
    $parents = array_merge($form['#parents'] ?? [], [$this->fieldDefinition->getName(), 'remote', 'url']);
    return implode('-', $parents) . '-add-button';
  }

  /**
   * Returns the oEmbed URL resolver.
   *
   * @return \Drupal\media\OEmbed\UrlResolverInterface
   *   The URL resolver.
   */
  protected static function getUrlResolver(): UrlResolverInterface {
    return \Drupal::service('media.oembed.url_resolver');
  }

  /**
   * Returns the oEmbed resource fetcher.
   *
   * @return \Drupal\media\OEmbed\ResourceFetcherInterface
   *   The resource fetcher.
   */
  protected static function getResourceFetcher(): ResourceFetcherInterface {
    return \Drupal::service('media.oembed.resource_fetcher');
  }

}
