<?php

namespace Drupal\media_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\TypedData\FieldItemDataDefinition;
use Drupal\Core\File\Exception\FileWriteException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media_widget\MediaWidgetBase;
use Drupal\media_widget\UpdatedMediaRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the Media Upload widget.
 *
 * @FieldWidget(
 *   id = "media_widget_upload",
 *   label = @Translation("Media upload widget"),
 *   multiple_values = TRUE,
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MediaUploadWidget extends MediaWidgetBase {

  /**
   * The element info manager.
   *
   * @var \Drupal\Core\Render\ElementInfoManagerInterface
   */
  protected $elementInfo;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * Constructs a new MediaUploadWidget.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info
   *   The element info manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The opener resolver.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository.
   */
  public function __construct(
    string $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager,
    UpdatedMediaRepository $updated_media_repository,
    ElementInfoManagerInterface $element_info,
    RendererInterface $renderer,
    FileSystemInterface $file_system,
    FileRepositoryInterface $file_repository,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $entity_type_manager, $updated_media_repository);

    $this->elementInfo = $element_info;
    $this->renderer = $renderer;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('media_widget.update_media_repository'),
      $container->get('element_info'),
      $container->get('renderer'),
      $container->get('file_system'),
      $container->get('file.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected static function getDefaultMediaFormMode(): string {
    return 'media_widget_upload';
  }

  /**
   * Builds the element for submitting source field value(s).
   *
   * @param array $element
   *   The widget form element.
   *
   * @return array
   *   The widget element, with the upload element added.
   *
   * @see ::processUploadElement()
   * @see ::validateUploadElement()
   * @see ::processInputValues()
   */
  protected function buildInputElement(array $element, FormStateInterface $form_state, array $form): array {
    $media_types = static::getMediaTypes($this->fieldDefinition);
    $state = $this->getState($form, $form_state);

    $cardinality = $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getCardinality();

    $process = (array) $this->elementInfo->getInfoProperty('managed_file', '#process', []);
    $process[] = [static::class, 'processUploadElement'];

    $element['upload'] = [
      '#type' => 'managed_file',
      '#title' => $this->formatPlural($cardinality, 'Add file', 'Add files'),
      '#required' => $this->fieldDefinition->isRequired() && empty($state['items']),
      '#process' => $process,
      '#upload_validators' => $this->getMergedValidators($media_types),
      '#multiple' => $cardinality > 1 || $cardinality === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      '#cardinality' => $cardinality,
      '#field_name' => $this->fieldDefinition->getName(),
    ];

    $file_upload_help = [
      '#theme' => 'file_upload_help',
      '#upload_validators' => $element['upload']['#upload_validators'],
      '#cardinality' => $cardinality,
    ];

    // The file upload help needs to be rendered since the description does not
    // accept render arrays. The FileWidget::formElement() method adds the file
    // upload help in the same way, so any theming improvements made to file
    // fields would also be applied to this upload field.
    // @see \Drupal\file\Plugin\Field\FieldWidget\FileWidget::formElement()
    $element['upload']['#description'] = $this->renderer->renderPlain($file_upload_help);

    $element['#element_validate'][] = [static::class, 'validateUploadElement'];

    return $element;
  }

  /**
   * Merges all upload validators into a single array.
   *
   * To be able support uploading files for multiple media types, it is
   * necessary to merge all the upload validators defined by all supported media
   * types. This will merge the upload validator arrays and will attempt to
   * alter known validator definitions so they act as a "union" of all the
   * individual definitions.
   *
   * During validation the specific validators defined by each media type will
   * still be applied, to ensure that they match the expected settings.
   *
   * For instance, a media reference field might be to support both images and
   * documents. Images may have a 1MB as maximum size while documents just 10KB.
   * This will allow to upload any file up to 1MB, but then any document larger
   * then 10KB would fail validation.
   *
   * @param \Drupal\media\MediaTypeInterface[] $media_types
   *   An array of supported media types.
   *
   * @return array[]
   *   An associative array of upload validator info, as expected by
   *   "file_validate".
   *
   * @throws \LogicException
   *   If the media types define the same validators, but with different
   *   arguments and merging is not supported for them.
   *
   * @see ::validateUploadElement()
   * @see \file_validate()
   */
  protected function getMergedValidators(array $media_types): array {
    $validators = [];

    foreach ($media_types as $media_type) {
      // Create a file item to get the upload validators.
      $item = static::createFileItem($media_type);
      foreach ($item->getUploadValidators() as $item_validator => $args) {
        if (!isset($validators[$item_validator])) {
          $validators[$item_validator] = $args;
          continue;
        }

        switch ($item_validator) {
          case 'FileSizeLimit':
            $validators[$item_validator] = ['fileLimit' => max(reset($validators[$item_validator]), reset($args))];
            break;

          case 'FileExtension':
            $validators[$item_validator] = ['extensions' => reset($validators[$item_validator]) . ' ' . reset($args)];
            break;

          default:
            if ($args !== $validators[$item_validator]) {
              throw new \LogicException('Unsupported widget configuration, the configured media types are not compatible.');
            }
        }
      }
    }

    return $validators;
  }

  /**
   * Processes an upload (managed_file) element.
   *
   * @param array $element
   *   The upload element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The processed upload element.
   */
  public static function processUploadElement(array $element, FormStateInterface $form_state): array {
    // The form '#parents' key is not reliable in this context, since it may not
    // be the same item passed in ::formElement().
    $parents = array_slice($element['#parents'], 0, -2);
    $state = static::getWidgetState($parents, $element['#field_name'], $form_state);

    $element['upload_button']['#limit_validation_errors'] = [
      [$element['#field_name'], 'upload'],
    ];
    $element['upload_button']['#ajax'] = [
      'callback' => [static::class, 'updateWidgetElement'],
      'wrapper' => $state['ajax_wrapper_id'],
    ];
    $element['upload_button']['#field_name'] = $element['#field_name'];

    // We need to get rid of the hidden "fids" item, otherwise the upload will
    // be processed at each form rebuild.
    if (isset($element['fids'])) {
      $element['fids']['#access'] = FALSE;
      $element = static::hideExtraSourceFieldComponents($element);
    }

    return $element;
  }

  /**
   * Processes an image or file source field element.
   *
   * @param array $element
   *   The entity form source field element.
   *
   * @return array
   *   The processed form element.
   */
  public static function hideExtraSourceFieldComponents($element): array {
    // Remove original button added by ManagedFile::processManagedFile().
    if (!empty($element['remove_button'])) {
      $element['remove_button']['#access'] = FALSE;
    }
    // Remove preview added by ImageWidget::process().
    if (!empty($element['preview'])) {
      $element['preview']['#access'] = FALSE;
    }

    $element['#title_display'] = 'none';
    $element['#description_display'] = 'none';

    // Remove the filename display.
    foreach ($element['#files'] as $file) {
      $element['file_' . $file->id()]['#access'] = FALSE;
    }

    return $element;
  }

  /**
   * {@inheritdoc }
   */
  protected function buildEntityFormElement(MediaInterface $media, array $form, FormStateInterface $form_state, int $delta): array {
    $element = parent::buildEntityFormElement($media, $form, $form_state, $delta);

    $source_field = $this->getSourceFieldName($media->get('bundle')->entity);
    if (isset($element['fields'][$source_field])) {
      $element['fields'][$source_field]['widget'][0]['#process'][] = [static::class, 'hideExtraSourceFieldComponents'];
    }

    return $element;
  }

  /**
   * Validates the upload element.
   *
   * @param array $element
   *   The form element to be validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The complete form array.
   */
  public static function validateUploadElement(array $element, FormStateInterface $form_state, array $form): void {
    $field_name = $element['#field_name'];
    $parents = array_merge($form['#parents'], [$field_name, 'upload']);
    $file_ids = NestedArray::getValue($form_state->getValues(), $parents) ?: [];

    // Check whether cardinality constraint are met.
    $cardinality = $element['upload']['#cardinality'];
    if ($cardinality !== FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      $state = static::getWidgetState($form['#parents'], $element['#field_name'], $form_state);
      $media_items = $state['items'] ?? [];
      if (count($file_ids) + count($media_items) > $cardinality) {
        $form_state->setError($element['upload'], new TranslatableMarkup('A maximum of @count files can be uploaded.', ['@count' => $cardinality]));
      }
    }

    // Apply upload validators configured for each media type actually applying
    // to uploaded file.
    if ($file_ids) {
      $errors = [];
      $field_definition = static::getFieldDefinition($element, $form_state);
      $media_types = static::getMediaTypes($field_definition);

      $storage = \Drupal::entityTypeManager()->getStorage('file');
      $files = $storage->loadMultiple($file_ids);

      foreach ($files as $file) {
        assert($file instanceof FileInterface);
        $media_type = static::getMediaTypeFromFile($file, $media_types);
        $item = static::createFileItem($media_type);
        $file_errors = file_validate($file, $item->getUploadValidators());
        $errors = array_merge($errors, $file_errors);
      }

      if ($errors) {
        $storage->delete($files);
        $message = implode(', ', $errors);
        $form_state->setError($element['upload'], PlainTextOutput::renderFromHtml($message));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function processInputValues(array $values, array $form, FormStateInterface $form_state): array {
    $added_items = [];

    $fids = $values['upload'] ?? [];
    if (!$fids) {
      return $added_items;
    }

    /** @var \Drupal\file\FileInterface[] $files */
    $files = $this->entityTypeManager
      ->getStorage('file')
      ->loadMultiple($values['upload']);

    $media_types = static::getMediaTypes($this->fieldDefinition);
    foreach ($files as $file) {
      $media_type = static::getMediaTypeFromFile($file, $media_types);
      $added_items[] = $this->createMediaFromValue($media_type, $file);
    }

    return $added_items;
  }

  /**
   * {@inheritdoc}
   */
  protected function needsRebuilding(array $form, FormStateInterface $form_state): bool {
    return TRUE;
  }

  /**
   * Returns the media type associated with the specified file.
   *
   * @param \Drupal\file\FileInterface $file
   *   An uploaded file.
   * @param array $media_types
   *   An array of supported media types.
   *
   * @return \Drupal\media\MediaTypeInterface
   *   The media type matching the specified file.
   *
   * @throws \InvalidArgumentException
   */
  protected static function getMediaTypeFromFile(FileInterface $file, array $media_types): MediaTypeInterface {
    $info = pathinfo($file->getFilename());
    $extension = strtolower($info['extension']);

    foreach ($media_types as $media_type) {
      $setting = $media_type->getSource()
        ->getSourceFieldDefinition($media_type)
        ->getSetting('file_extensions');

      $extensions = preg_split('/\s+/', $setting);
      if (in_array($extension, $extensions)) {
        return $media_type;
      }
    }

    throw new \InvalidArgumentException(sprintf('Invalid media upload: %s', $file->getFilename()));
  }

  /**
   * Creates a new, unsaved media item from a source field value.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type of the media item.
   * @param mixed $value
   *   The value for the source field of the media item.
   *
   * @return \Drupal\media\MediaInterface
   *   An unsaved media entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createMediaFromValue(MediaTypeInterface $media_type, $value): MediaInterface {
    if (!($value instanceof FileInterface)) {
      throw new \InvalidArgumentException('Cannot create a media item without a file entity.');
    }

    // Create a file item to get the upload location.
    $file = $value;
    if ($file->isTemporary()) {
      $item = static::createFileItem($media_type);
      $upload_location = $item->getUploadLocation();
      if (!$this->fileSystem->prepareDirectory($upload_location, FileSystemInterface::CREATE_DIRECTORY)) {
        throw new FileWriteException("The destination directory '$upload_location' is not writable");
      }
      $file = $this->fileRepository->move($file, $upload_location);
    }

    return parent::createMediaFromValue($media_type, $file);
  }

  /**
   * Create a file field item.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type of the media item.
   *
   * @return \Drupal\file\Plugin\Field\FieldType\FileItem
   *   A created file item.
   */
  protected static function createFileItem(MediaTypeInterface $media_type): FileItem {
    $field_definition = $media_type->getSource()->getSourceFieldDefinition($media_type);
    $data_definition = FieldItemDataDefinition::create($field_definition);
    return new FileItem($data_definition);
  }

  /**
   * {@inheritdoc}
   */
  protected static function isSupportedMediaType(MediaTypeInterface $media_type): bool {
    // The file upload form only supports media types which use a file field
    // as a source field.
    $source_field_definition = $media_type->getSource()->getSourceFieldDefinition($media_type);
    return is_a($source_field_definition->getClass(), FileFieldItemList::class, TRUE);
  }

}
